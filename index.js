import childProcess from 'child_process';

import signalCliWrapper from 'signal-cli-wrapper';

export default ({ signalCliPath }) => signalCliWrapper({
    execute: args => new Promise(resolve => childProcess.exec(
        [signalCliPath, ...args].join(' '),
        (_, stdout, stderr) => resolve([stdout.trim(), stderr.trim()].join('\n').trim())
    )),
    spawn: ({
        args,
        onData
    }) => {
        const command = childProcess.spawn(signalCliPath, args);
        command.stdout.on('data', data => onData(data.toString().trim()));
        command.stderr.on('data', data => onData(data.toString().trim()));
    }
});